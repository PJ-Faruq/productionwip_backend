﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductionWIP.Models.DTO
{
    public class HeatTreatSearchDto
    {
        public int PageNumber { get; set; }

        public string JobCardNumber { get; set; } = "";

        public string FirstName { get; set; } = "";


        public string StationNumber { get; set; } = "";

        public string Temperature { get; set; } = "";

        public string[] Date { get; set; }

        public string Load { get; set; } = "";

        public string OperatorName { get; set; } = "";


    }
}
