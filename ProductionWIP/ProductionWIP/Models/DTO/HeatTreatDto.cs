﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ProductionWIP.Models.DTO
{
    public class HeatTreatDto
    {
        public int Id { get; set; }
        public string JobCardNumber { get; set; }

        public string FirstName { get; set; }


        public string StationNumber { get; set; }

        public string Temperature { get; set; }

        public string Date { get; set; }

        public string Load { get; set; }

        public string Image { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Image4 { get; set; }
        public string OperatorName { get; set; }
        public bool IsImageVisible { get; set; } = true;

    }
}
