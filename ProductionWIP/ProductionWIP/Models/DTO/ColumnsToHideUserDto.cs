﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductionWIP.Models.DTO
{
    public class ColumnsToHideUserDto
    {
        public List<int> ColumnId { get; set; }
        public int OperatorId { get; set; }
    }
}
