﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductionWIP.Models
{
    public class HeatTreat
    {
        public int Id { get; set; }



        public string JobCardNumber { get; set; }

        public string FirstName { get; set; }


        public string StationNumber { get; set; }

        public string Temperature { get; set; }

        public DateTime Date { get; set; }

        public string Load { get; set; }

        public string Image { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Image4 { get; set; }



        public int? OperatorId { get; set; }

        public Operator Operator { get; set; }
    }
}
