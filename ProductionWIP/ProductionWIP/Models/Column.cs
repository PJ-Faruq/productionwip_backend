﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductionWIP.Models
{
    public class Column
    {
        public int Id { get; set; }
        public int Order { get; set; }
        public string Label { get; set; }

    }
}
