﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProductionWIP.Models
{
    public class Operator
    {
        public int Id { get; set; }
        [Required]
        public string Number { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
