﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductionWIP.Models
{
    public class EmbedCode
    {
        public int Id { get; set; }
        public string Identifier { get; set; }
        public string Token { get; set; }

    }
}
