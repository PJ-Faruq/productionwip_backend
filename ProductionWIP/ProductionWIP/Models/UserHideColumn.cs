﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductionWIP.Models
{
    public class UserHideColumn
    {
        public int Id { get; set; }

        public int OperatorId { get; set; }

        public Operator Operators { get; set; }

        public int ColumnId { get; set; }

        public Column Column{ get; set; }
    }
}
