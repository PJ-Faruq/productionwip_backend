﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProductionWIP.Migrations
{
    public partial class HeatTreat_Date_Datatype_Changes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Date",
                table: "HeatTreats",
                nullable: true,
                oldClrType: typeof(DateTime));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Date",
                table: "HeatTreats",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
