﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProductionWIP.Migrations
{
    public partial class HeatTreat_DateType_Change : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Date",
                table: "HeatTreats",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Date",
                table: "HeatTreats",
                nullable: true,
                oldClrType: typeof(DateTime));
        }
    }
}
