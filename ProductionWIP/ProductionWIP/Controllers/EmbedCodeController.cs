﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductionWIP.Models;
using ProductionWIP.Repository;

namespace ProductionWIP.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class EmbedCodeController : ControllerBase
    {
        private readonly EmbedCodeRepo embedCodeRepo;

        public EmbedCodeController(EmbedCodeRepo embedCodeRepo)
        {
            this.embedCodeRepo = embedCodeRepo;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] EmbedCode embedCode)
        {
            try
            {
                bool status = false;
                status = await embedCodeRepo.Create(embedCode);
                return Ok(status);

            }
            catch (Exception)
            {
                return StatusCode(500, "Some Error Occured");
            }

        }

        public async Task<IActionResult> GetAll()
        {
            try
            {
                List<EmbedCode> listOfEmbedCode = await embedCodeRepo.GetAll();
                return Ok(listOfEmbedCode);
            }
            catch (Exception)
            {
                return StatusCode(500, "Some Error Occured");
            }

        }


        [AllowAnonymous]
        [HttpGet("{token}")]
        public async Task<IActionResult> getByToken(string token)
        {
            try
            {
                bool status = await embedCodeRepo.getByToken(token);
                return Ok(status);
            }
            catch (Exception)
            {
                return StatusCode(500, "Some Error Occured");
            }

        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] EmbedCode embedCode)
        {
            try
            {
                bool status = false;
                status = await embedCodeRepo.Update(embedCode);
                return Ok(status);

            }
            catch (Exception e)
            {
                return StatusCode(500, "Some Error Occured");
            }

        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                bool status = false;
                status = await embedCodeRepo.Delete(id);
                return Ok(status);

            }
            catch (Exception e)
            {
                return NotFound();
            }

        }
    }
}