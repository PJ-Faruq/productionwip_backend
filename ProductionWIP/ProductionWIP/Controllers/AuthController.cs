﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ProductionWIP.Data.DTO;
using ProductionWIP.Models;
using ProductionWIP.Models.DTO;
using ProductionWIP.Repository;

namespace ProductionWIP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration config;
        private readonly UserManager<AppUser> userManager;
        private readonly SignInManager<AppUser> signInManager;
        private readonly OperatorRepo operatorRepo;

        public AuthController(IConfiguration config,
            UserManager<AppUser> userManager, SignInManager<AppUser> signInManager,OperatorRepo operatorRepo)
        {
            this.config = config;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.operatorRepo = operatorRepo;
        }
////For Creating Admin
//        [HttpGet]
//        [Route("Register")]
//        public async Task<IActionResult> Register()
//        {
//            try
//            {
//                AppUser user = new AppUser();
//                user.UserName = "Admin";

//                var result = await userManager.CreateAsync(user, "123456");
//                await userManager.AddToRoleAsync(user, "Admin");
//                return Ok(result);
//            }
//            catch (Exception)
//            {

//                return Unauthorized();
//            }

//        }

        [HttpPost("AdminLogin")]
        public async Task<IActionResult> AdminLogin(LoginDto login)
        {
            try
            {
                var user = await userManager.FindByNameAsync(login.UserName);

                if (user == null)
                {
                    return Ok(false);
                }

                var result = await signInManager.CheckPasswordSignInAsync(user, login.Password, false);

                if (result.Succeeded)
                {
                    var appUser = userManager.Users.FirstOrDefault(x => x.NormalizedUserName == login.UserName.ToUpper());
                    string token = await GenerateToken(appUser);
                    return Ok(new
                    {
                        token
                    });
                }
                else
                {
                    return Ok(false);
                }

                
            }
            catch (Exception)
            {

                return Unauthorized();
            }
            

        }

        [HttpPost("OperatorLogin")]
        public async Task<IActionResult> OperatorLogin(OperatorLoginDto login)
        {
            try
            {
                Operator user = operatorRepo.GetByNumber(login.Number);
                if (user != null)
                {
                    string token = await GenerateTokenForOperator(user);
                    return Ok(new
                    {
                        token
                    });
                }

                else
                {
                    return Ok(false);
                }
            }
            catch (Exception)
            {

                return Unauthorized();
            }
            

        }

        
        [HttpPost("ChangeAdminPassword")]
        public async Task<IActionResult> ChangeAdminPassword(ChangePasswordDto model)
        {
            try
            {
                
                var user = await userManager.FindByNameAsync("Admin");
                var result = await signInManager.CheckPasswordSignInAsync(user, model.CurrentPassword, false);
                
                if (result.Succeeded)
                {
                   var isChange= await userManager.ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword);
                    if (isChange.Succeeded)
                    {
                        return Ok(true);
                    }
                }

                else
                {
                    return Ok(false);
                }

               return StatusCode(500, "Some Error Occured");
            }
            catch (Exception)
            {

                return Unauthorized();
            }


        }

        private async Task<string> GenerateToken(AppUser user)
        {
            //Creating Jwt Token
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.NameIdentifier,user.Id.ToString()),
                new Claim(ClaimTypes.Name,user.UserName),
                new Claim(ClaimTypes.Role,"Admin"),
            };

            //var roles = await userManager.GetRolesAsync(user);
            //foreach (var role in roles)
            //{
            //    claims.Add(new Claim(ClaimTypes.Role, role));
            //}


            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config.GetSection("Application:Token").Value));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(7),
                SigningCredentials = creds
            };

            var tokenHadler = new JwtSecurityTokenHandler();
            var token = tokenHadler.CreateToken(tokenDescriptor);
            return tokenHadler.WriteToken(token);

        }


        private async Task<string> GenerateTokenForOperator(Operator user)
        {
            //Creating Jwt Token
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.NameIdentifier,user.Id.ToString()),
                new Claim(ClaimTypes.Name,user.Name),
                new Claim(ClaimTypes.SerialNumber,user.Number),
                new Claim(ClaimTypes.Role,"Operator")
            };


            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config.GetSection("Application:Token").Value));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(7),
                SigningCredentials = creds
            };

            var tokenHadler = new JwtSecurityTokenHandler();
            var token = tokenHadler.CreateToken(tokenDescriptor);
            return tokenHadler.WriteToken(token);

        }
    }
}