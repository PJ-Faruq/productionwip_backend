﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductionWIP.Models;
using ProductionWIP.Repository;

namespace ProductionWIP.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class OperatorController : ControllerBase
    {
        private readonly OperatorRepo operatorRepo;

        public OperatorController(OperatorRepo operatorRepo)
        {
            this.operatorRepo = operatorRepo;
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Operator oprtor)
        {
            try
            {
                bool status = false;
                status= await operatorRepo.Create(oprtor);
                return Ok(status);

            }
            catch (Exception)
            {
                return StatusCode(500, "Some Error Occured");
            }

        }

        public async Task<IActionResult> GetAll()
        {
            try
            {
               List<Operator> listOfOptr = await operatorRepo.GetAll();
                return Ok(listOfOptr);
            }
            catch (Exception)
            {
                return StatusCode(500, "Some Error Occured");
            }

        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] Operator oprtor)
        {
            try
            {
                bool status = false;
                status = await operatorRepo.Update(oprtor); 
                return Ok(status);

            }
            catch (Exception e)
            {
                return StatusCode(500, "Some Error Occured");
            }

        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                bool status = false;
                status = await operatorRepo.Delete(id);
                return Ok(status);

            }
            catch (Exception e)
            {
                return NotFound();
            }

        }


    }
}