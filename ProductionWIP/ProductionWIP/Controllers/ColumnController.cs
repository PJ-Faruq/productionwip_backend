﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductionWIP.Models;
using ProductionWIP.Repository;

namespace ProductionWIP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ColumnController : ControllerBase
    {
        private readonly ColumnRepo columnRepo;

        public ColumnController(ColumnRepo columnRepo)
        {
            this.columnRepo = columnRepo;
        }
        [AllowAnonymous]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var request = Request.Headers;
                List<Column> columns = await columnRepo.GetAll();
                return Ok(columns);
            }
            catch (Exception)
            {

                return StatusCode(500, "Some Error Occured");
            }

        }
        [Authorize(Roles = "Admin")]
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] Column column)
        {
            try
            {
                bool status = false;
                status = await columnRepo.Update(column);
                return Ok(status);

            }
            catch (Exception e)
            {
                return StatusCode(500, "Some Error Occured");
            }

        }
    }
}