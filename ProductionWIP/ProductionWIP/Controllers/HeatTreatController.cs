﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductionWIP.Models;
using ProductionWIP.Models.DTO;
using ProductionWIP.Repository;

namespace ProductionWIP.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class HeatTreatController : ControllerBase
    {
        private readonly HeatTreatRepo heatTreatRepo;

        public HeatTreatController(HeatTreatRepo heatTreatRepo)
        {
            this.heatTreatRepo = heatTreatRepo;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Create()
        {
            try
            {
                HeatTreat heatTreat = new HeatTreat();

                heatTreat.FirstName = Request.Form["firstName"].ToString();
                heatTreat.JobCardNumber= Request.Form["jobCardNumber"].ToString();
                heatTreat.StationNumber = Request.Form["stationNumber"].ToString();
                heatTreat.Temperature = Request.Form["temperature"].ToString();
                heatTreat.Load = Request.Form["load"].ToString();
                if (Request.Form["operatorId"] != "")
                {
                    heatTreat.OperatorId = Convert.ToInt32(Request.Form["operatorId"].ToString());
                }
                else
                {
                    heatTreat.OperatorId = null;
                }

                heatTreat.Date = DateTime.Now;

                //Save image to Resources/Images folder and return the image path
                heatTreat.Image= SaveAndGetImagePath(Request.Form.Files["image"]);
                heatTreat.Image1= SaveAndGetImagePath(Request.Form.Files["image1"]);
                heatTreat.Image2 = SaveAndGetImagePath(Request.Form.Files["image2"]);
                heatTreat.Image3 = SaveAndGetImagePath(Request.Form.Files["image3"]);
                heatTreat.Image4 = SaveAndGetImagePath(Request.Form.Files["image4"]);


                bool status = false;

                status = await heatTreatRepo.Create(heatTreat);

                return Ok(heatTreat);

            }
            catch (Exception e)
            {

                return StatusCode(500, "Some Error Occured");
            }

        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public async Task<IActionResult> Update()
        {

            try
            {

                string itemId = Request.Form["id"].ToString();
                int id =Convert.ToInt32(Request.Form["id"].ToString());

                HeatTreat existingModel = heatTreatRepo.GetById(id);
                if (existingModel == null)
                {
                    return Ok(false);
                }


                HeatTreat heatTreat = new HeatTreat();

                heatTreat.Id = id;
                heatTreat.FirstName = Request.Form["firstName"].ToString();
                heatTreat.JobCardNumber = Request.Form["jobCardNumber"].ToString();
                heatTreat.StationNumber = Request.Form["stationNumber"].ToString();
                heatTreat.Temperature = Request.Form["temperature"].ToString();
                heatTreat.Load = Request.Form["load"].ToString();
                if (Request.Form["operatorId"] != "")
                {
                    heatTreat.OperatorId = Convert.ToInt32(Request.Form["operatorId"].ToString());
                }
                else
                {
                    heatTreat.OperatorId = null;
                }

                heatTreat.Date = existingModel.Date;

               

                if (Request.Form.Files["image"] != null)
                {

                    if(existingModel.Image != "")
                    {
                        DeleteExistingImage(existingModel.Image);
                        heatTreat.Image = SaveAndGetImagePath(Request.Form.Files["image"]);
                    }
                    else
                    {
                        heatTreat.Image = SaveAndGetImagePath(Request.Form.Files["image"]);
                    }

                }
                else
                {
                    if (Request.Form["image"].ToString() == "remove")
                    {
                        DeleteExistingImage(existingModel.Image);
                        heatTreat.Image = "";
                    }

                    else
                    {
                        heatTreat.Image = existingModel.Image;
                    }

                    
                }

                if (Request.Form.Files["image1"] != null)
                {

                    if (existingModel.Image1 != "")
                    {
                        DeleteExistingImage(existingModel.Image1);
                        heatTreat.Image1 = SaveAndGetImagePath(Request.Form.Files["image1"]);
                    }
                    else
                    {
                        heatTreat.Image1 = SaveAndGetImagePath(Request.Form.Files["image1"]);
                    }

                }
                else
                {
                    if (Request.Form["image1"].ToString() == "remove")
                    {
                        DeleteExistingImage(existingModel.Image1);
                        heatTreat.Image1 = "";
                    }

                    else
                    {
                        heatTreat.Image1 = existingModel.Image1;
                    }


                }

                if (Request.Form.Files["image2"] != null)
                {

                    if (existingModel.Image2 != "")
                    {
                        DeleteExistingImage(existingModel.Image2);
                        heatTreat.Image2 = SaveAndGetImagePath(Request.Form.Files["image2"]);
                    }
                    else
                    {
                        heatTreat.Image2 = SaveAndGetImagePath(Request.Form.Files["image2"]);
                    }

                }
                else
                {
                    if (Request.Form["image2"].ToString() == "remove")
                    {
                        DeleteExistingImage(existingModel.Image2);
                        heatTreat.Image2 = "";
                    }

                    else
                    {
                        heatTreat.Image2 = existingModel.Image2;
                    }


                }

                if (Request.Form.Files["Image3"] != null)
                {

                    if (existingModel.Image3 != "")
                    {
                        DeleteExistingImage(existingModel.Image3);
                        heatTreat.Image3 = SaveAndGetImagePath(Request.Form.Files["Image3"]);
                    }
                    else
                    {
                        heatTreat.Image3 = SaveAndGetImagePath(Request.Form.Files["Image3"]);
                    }

                }
                else
                {
                    if (Request.Form["Image3"].ToString() == "remove")
                    {
                        DeleteExistingImage(existingModel.Image3);
                        heatTreat.Image3 = "";
                    }

                    else
                    {
                        heatTreat.Image3 = existingModel.Image3;
                    }


                }

                if (Request.Form.Files["Image4"] != null)
                {

                    if (existingModel.Image4 != "")
                    {
                        DeleteExistingImage(existingModel.Image4);
                        heatTreat.Image4 = SaveAndGetImagePath(Request.Form.Files["Image4"]);
                    }
                    else
                    {
                        heatTreat.Image4 = SaveAndGetImagePath(Request.Form.Files["Image4"]);
                    }

                }
                else
                {
                    if (Request.Form["Image4"].ToString() == "remove")
                    {
                        DeleteExistingImage(existingModel.Image4);
                        heatTreat.Image4 = "";
                    }

                    else
                    {
                        heatTreat.Image4 = existingModel.Image4;
                    }


                }


                bool status = false;

                status = await heatTreatRepo.Update(heatTreat);

                HeatTreatDto heat = new HeatTreatDto();
                heat.Id = heatTreat.Id;
                heat.FirstName = heatTreat.FirstName;
                heat.JobCardNumber = heatTreat.JobCardNumber;
                heat.StationNumber = heatTreat.StationNumber;
                heat.Temperature = heatTreat.Temperature;
                heat.Date = heatTreat.Date.ToShortDateString() + " " + heatTreat.Date.ToShortTimeString();
                heat.Load = heatTreat.Load;
                heat.Image = heatTreat.Image;
                heat.Image1 = heatTreat.Image1;
                heat.Image2 = heatTreat.Image2;
                heat.Image3 = heatTreat.Image3;
                heat.Image4 = heatTreat.Image4;

                if (heatTreat.Image == "" && heatTreat.Image1 == "" && heatTreat.Image2 == "" && heatTreat.Image3 == "" && heatTreat.Image4 == "")
                {
                    heat.IsImageVisible = false;
                }

                return Ok(heat);

            }
            catch (Exception e)
            {

                return StatusCode(500, "Some Error Occured");
            }


        }

        private void DeleteExistingImage(string image)
        {
            string folderName = Path.Combine("Resources", "Images");
            string path = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            // Check if file exists with its full path    
            if (System.IO.File.Exists(Path.Combine(path, image)))
            {
                // If file found, delete it    
                System.IO.File.Delete(Path.Combine(path, image));

            }
        }

        [Authorize]
        [HttpPost]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll(HeatTreatSearchDto heatTreatSearchDto)
        {
            try
            {
                
                List<HeatTreat> listOfHeatTreat = await heatTreatRepo.GetAll(heatTreatSearchDto);
                List<HeatTreatDto> finalList = PrepareImageFullPath(listOfHeatTreat);

                return Ok(finalList);
            }
            catch (Exception)
            {
                return StatusCode(500, "Some Error Occured");
            }

        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                HeatTreat existingModel = heatTreatRepo.GetById(id);
                if (existingModel == null)
                {
                    return Ok(false);
                }

                if (existingModel.Image != "")
                {
                    DeleteExistingImage(existingModel.Image);
                }

                if (existingModel.Image1 != "")
                {
                    DeleteExistingImage(existingModel.Image1);
                }
                if (existingModel.Image2 != "")
                {
                    DeleteExistingImage(existingModel.Image2);
                }
                if (existingModel.Image3 != "")
                {
                    DeleteExistingImage(existingModel.Image3);
                }

                if (existingModel.Image4 != "")
                {
                    DeleteExistingImage(existingModel.Image4);
                }

                bool status = false;
                status = await heatTreatRepo.Delete(id);
                return Ok(status);

            }
            catch (Exception e)
            {
                return NotFound();
            }

        }

        private List<HeatTreatDto> PrepareImageFullPath(List<HeatTreat> listOfHeatTreat)
        {
            List<HeatTreatDto> preparedList = new List<HeatTreatDto>();

            foreach (var item in listOfHeatTreat)
            {
                HeatTreatDto heatTreat = new HeatTreatDto();
                heatTreat.Id = item.Id;
                heatTreat.FirstName = item.FirstName;
                heatTreat.JobCardNumber = item.JobCardNumber;
                heatTreat.StationNumber = item.StationNumber;
                heatTreat.Temperature = item.Temperature;
                heatTreat.Date =item.Date.ToShortDateString()+" "+ item.Date.ToShortTimeString();
                heatTreat.Load = item.Load;
                heatTreat.Image = item.Image;
                heatTreat.Image1 = item.Image1;
                heatTreat.Image2 = item.Image2;
                heatTreat.Image3 = item.Image3;
                heatTreat.Image4 = item.Image4;

                if(item.Image=="" && item.Image1 == "" && item.Image2 == "" && item.Image3 == "" && item.Image4 == "")
                {
                    heatTreat.IsImageVisible = false;
                }


                preparedList.Add(heatTreat);

            }

            return preparedList;
        }

        private string SaveAndGetImagePath(IFormFile image)
        {
            var file = image;
            var folderName = Path.Combine("Resources", "Images");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

            if (file !=null)
            {
                var fileName = Guid.NewGuid().ToString() + "_" + file.FileName;
                //var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                var fullPath = Path.Combine(pathToSave, fileName);
                var dbPath = Path.Combine(folderName, fileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                return fileName;
            }
            else
            {
                return "";
            }
        }


        [HttpGet]
        [Route("GetColumnsToHideUser/{operatorId}")]
        public async Task<IActionResult> GetColumnsToHideUser(int operatorId)
        {
            try
            {
                List<UserHideColumn> columns = await heatTreatRepo.GetColumnsToHideUser(operatorId);
                return Ok(columns);
            }
            catch (Exception)
            {

                return StatusCode(500, "Some Error Occured");
            }

        }

        [HttpPost]
        [Route("CreateColumnsToHideUser")]
        public async Task<IActionResult> CreateColumnsToHideUser(ColumnsToHideUserDto columnsToHide)
        {
            try
            {
                bool status = false;
                if (columnsToHide.ColumnId != null)
                {
                     List<UserHideColumn> listOfUserHideColumn = new List<UserHideColumn>();

                    //If  Column found for hide
                    if (columnsToHide.ColumnId.Count > 0)
                    {
                        foreach (var item in columnsToHide.ColumnId)
                        {
                            UserHideColumn userHide = new UserHideColumn();
                            userHide.ColumnId = item;
                            userHide.OperatorId = columnsToHide.OperatorId;
                            listOfUserHideColumn.Add(userHide);
                        }
                    }
                    
                    status = await heatTreatRepo.CreateColumnsToHideUser(listOfUserHideColumn,columnsToHide.OperatorId);

                }
                return Ok(status);


            }
            catch (Exception e)
            {

                return StatusCode(500, "Some Error Occured");
            }

        }
    }
}