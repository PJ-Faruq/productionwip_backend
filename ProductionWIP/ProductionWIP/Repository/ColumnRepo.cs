﻿using Microsoft.EntityFrameworkCore;
using ProductionWIP.Data;
using ProductionWIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductionWIP.Repository
{
    public class ColumnRepo
    {
        private readonly ProductionWipDbContext dbContext;

        public ColumnRepo(ProductionWipDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        internal async Task<bool> Update(Column column)
        {
            dbContext.Entry(column).State = EntityState.Modified;
            return await dbContext.SaveChangesAsync() > 0;
        }

        internal async Task<List<Column>> GetAll()
        {
            return await dbContext.Columns.ToListAsync();
        }
    }
}
