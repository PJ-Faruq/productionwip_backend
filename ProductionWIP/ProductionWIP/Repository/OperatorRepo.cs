﻿using Microsoft.EntityFrameworkCore;
using ProductionWIP.Data;
using ProductionWIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductionWIP.Repository
{
    public class OperatorRepo
    {
        private readonly ProductionWipDbContext dbContext;

        public OperatorRepo(ProductionWipDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        internal async Task<bool> Create(Operator oprator)
        {
            await dbContext.Operators.AddAsync(oprator);
            return dbContext.SaveChanges() > 0;

        }

        internal async Task<List<Operator>> GetAll()
        {
            return await dbContext.Operators.OrderByDescending(x=>x.Id).ToListAsync();
        }

        internal async Task<bool> Update(Operator oprtr)
        {
            dbContext.Entry(oprtr).State = EntityState.Modified;
            return await dbContext.SaveChangesAsync() > 0;
        }

        internal async Task<bool> Delete(int id)
        {
            bool status = false;
            Operator optr = dbContext.Operators.Find(id);
            if (optr != null)
            {
                dbContext.Operators.Remove(optr);
                status= await dbContext.SaveChangesAsync() > 0;
            }

            return status;

        }

        internal Operator GetByNumber(string number)
        {
            Operator oprtr = dbContext.Operators.FirstOrDefault(x => x.Number == number);
            return oprtr;
        }
    }
}
