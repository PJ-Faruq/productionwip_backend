﻿using Microsoft.EntityFrameworkCore;
using ProductionWIP.Data;
using ProductionWIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductionWIP.Repository
{
    public class EmbedCodeRepo
    {
        private readonly ProductionWipDbContext dbContext;

        public EmbedCodeRepo(ProductionWipDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        internal async Task<bool> Create(EmbedCode embedCode)
        {
            await dbContext.EmbedCodes.AddAsync(embedCode);
            return dbContext.SaveChanges() > 0;

        }

        internal async Task<List<EmbedCode>> GetAll()
        {
            return await dbContext.EmbedCodes.OrderByDescending(x => x.Id).ToListAsync();
        }

        internal async Task<bool> Update(EmbedCode embed)
        {
            dbContext.Entry(embed).State = EntityState.Modified;
            return await dbContext.SaveChangesAsync() > 0;
        }

        internal async Task<bool> Delete(int id)
        {
            bool status = false;
            EmbedCode embed = dbContext.EmbedCodes.Find(id);
            if (embed != null)
            {
                dbContext.EmbedCodes.Remove(embed);
                status = await dbContext.SaveChangesAsync() > 0;
            }

            return status;

        }

        internal async Task<bool> getByToken(string token)
        {
            EmbedCode embed =await dbContext.EmbedCodes.FirstOrDefaultAsync(x => x.Token == token);
            if (embed != null)
            {
                return true;
            }
            return false;
        }
    }
}
