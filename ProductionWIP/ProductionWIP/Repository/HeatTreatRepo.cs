﻿using Microsoft.EntityFrameworkCore;
using ProductionWIP.Data;
using ProductionWIP.Models;
using ProductionWIP.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductionWIP.Repository
{
    public class HeatTreatRepo
    {
        private readonly ProductionWipDbContext dbContext;

        public HeatTreatRepo(ProductionWipDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        internal async Task<List<UserHideColumn>> GetColumnsToHideUser(int operatorId)
        {
            return await dbContext.UserHideColumns.Where(x=>x.OperatorId==operatorId).ToListAsync();
        }

        internal async Task<bool> CreateColumnsToHideUser(List<UserHideColumn> listOfUserHideColumn,int operatorId)
        {
            if (listOfUserHideColumn.Count == 0)
            {
                RemoveRange(operatorId);
                return true;
            }

             RemoveRange(operatorId);

             bool status = false;
             dbContext.UserHideColumns.AddRange(listOfUserHideColumn);
             status= await dbContext.SaveChangesAsync() > 0;
             return status;

        }

        private void RemoveRange(int operatorId)
        {
            List<UserHideColumn> columns = dbContext.UserHideColumns.Where(x => x.OperatorId == operatorId).ToList();
            dbContext.UserHideColumns.RemoveRange(columns);
            dbContext.SaveChanges();
        }

        internal async Task<bool> Create(HeatTreat heatTreat)
        {
            
            dbContext.HeatTreats.Add(heatTreat);
            return await dbContext.SaveChangesAsync() > 0;

            
        }

        internal Task<List<HeatTreat>> GetAll(HeatTreatSearchDto heatTreatSearchDto)
        {
            var list = dbContext.HeatTreats.AsNoTracking().AsQueryable();
            if (heatTreatSearchDto.FirstName != "")
            {
                list = list.Where(x => x.FirstName.Contains(heatTreatSearchDto.FirstName)).AsQueryable();
            }
            if (heatTreatSearchDto.JobCardNumber != "")
            {
                list = list.Where(x => x.JobCardNumber.Contains(heatTreatSearchDto.JobCardNumber)).AsQueryable();
            }
            if (heatTreatSearchDto.StationNumber != "")
            {
                list = list.Where(x => x.StationNumber.Contains(heatTreatSearchDto.StationNumber)).AsQueryable();
            }
            if (heatTreatSearchDto.Temperature != "")
            {
                list = list.Where(x => x.Temperature.Contains(heatTreatSearchDto.Temperature)).AsQueryable();
            }
            if (heatTreatSearchDto.Load != "")
            {
                list = list.Where(x => x.Load.Contains(heatTreatSearchDto.Load)).AsQueryable();
            }
            if (heatTreatSearchDto.OperatorName != "")
            {
                list = list.Where(x => x.Operator.Name.Contains(heatTreatSearchDto.OperatorName)).AsQueryable();
            }
            if (heatTreatSearchDto.Date[0]!="" && heatTreatSearchDto.Date[1] != "")
            {
                DateTime fromDate = Convert.ToDateTime(heatTreatSearchDto.Date[0]);
                DateTime toDate= Convert.ToDateTime(heatTreatSearchDto.Date[1]);

                list = list.Where(x => x.Date.Year >= fromDate.Year && x.Date.Month>=fromDate.Month && x.Date.Day >= fromDate.Day &&
                                   x.Date.Year<=toDate.Year && x.Date.Month <=toDate.Month && x.Date.Day <=toDate.Day).AsQueryable();
            }

            int pageNumber = heatTreatSearchDto.PageNumber;


            return list.OrderByDescending(x=>x.Id).Skip(pageNumber*10).Take(10).ToListAsync();
        }

        internal async Task<bool> Delete(int id)
        {
            bool status = false;
            HeatTreat heatTreat = dbContext.HeatTreats.Find(id);
            if (heatTreat != null)
            {
                dbContext.HeatTreats.Remove(heatTreat);
                status = await dbContext.SaveChangesAsync() > 0;
            }

            return status;
        }

        internal async Task<bool> Update(HeatTreat heatTreat)
        {
            dbContext.Entry(heatTreat).State = EntityState.Modified;
            return await dbContext.SaveChangesAsync() > 0;
        }

        internal  HeatTreat GetById(int id)
        {
            return  dbContext.HeatTreats.AsNoTracking().FirstOrDefault(x => x.Id == id);
        }


    }
}
