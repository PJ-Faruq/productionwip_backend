﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ProductionWIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductionWIP.Data
{
    public class ProductionWipDbContext:IdentityDbContext
    {
        public ProductionWipDbContext(DbContextOptions<ProductionWipDbContext> options) : base(options) { }

        public DbSet<AppUser> AppUsers { get; set; }
        public DbSet <Column> Columns { get; set; }
        public DbSet<Operator> Operators { get; set; }
        public DbSet<UserHideColumn> UserHideColumns { get; set; }
        public DbSet<HeatTreat> HeatTreats { get; set; }
        public DbSet<EmbedCode> EmbedCodes { get; set; }

    }
}
